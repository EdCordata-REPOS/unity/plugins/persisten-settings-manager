# Persisten Settings Manager
Persisten Settings Manager, by EdCordata
<br/>
[gitlab.com/EdCordata-REPOS/unity/plugins/persisten-settings-manager](https://gitlab.com/EdCordata-REPOS/unity/plugins/persisten-settings-manager)


<br/>


## Description
Simple class to save/load class to json file in user folder, or use default file.
User file is created, if missing.


<br/>


## Installation
Add file [src/PersistentSettingsManager.cs](src/PersistentSettingsManager.cs) to your project.


<br/>


## Usage

Load:
```c#
new PersistentSettingsManager() {
	classInstance       = this, // class instance, that is saved in the json file
	defaultSettingsPath = "/full/path/to/default/settings/file.json",
	userSettingsPath    = "/full/path/to/user/settings/file.json"
}.Load();
```

Save:
```c#
new PersistentSettingsManager() {
	classInstance       = this, // class instance, that is saved in the json file
	defaultSettingsPath = "/full/path/to/default/settings/file.json",
	userSettingsPath    = "/full/path/to/user/settings/file.json"
}.Save();
```

Reset:
```c#
new PersistentSettingsManager() {
	classInstance       = this, // class instance, that is saved in the json file
	defaultSettingsPath = "/full/path/to/default/settings/file.json",
	userSettingsPath    = "/full/path/to/user/settings/file.json"
}.Reset();
```


<br/>


## License
This project is licensed under
[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) license,
which means this plugin is free for personal use only.

Contact info:
<br/>
[https://github.com/EdCordata](https://github.com/EdCordata)
