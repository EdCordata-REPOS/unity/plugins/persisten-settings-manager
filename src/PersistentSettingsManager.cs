using UnityEngine;
using System.IO;


/*

  Simple class to save/load class to json file in user folder, or use default file.
  User file is created, if missing.

  JSON file format:
  { "attributeName": "Attrubute Value" }

  Load:
    new PersistentSettingsManager() {
        classInstance       = this, // class instance, that is saved in the json file
        defaultSettingsPath = "/full/path/to/default/settings/file.json",
        userSettingsPath    = "/full/path/to/user/settings/file.json"
    }.Load();

  Save:
    new PersistentSettingsManager() {
        classInstance       = this, // class instance, that is saved in the json file
        defaultSettingsPath = "/full/path/to/default/settings/file.json",
        userSettingsPath    = "/full/path/to/user/settings/file.json"
    }.Save();

  Reset:
    new PersistentSettingsManager() {
        classInstance       = this, // class instance, that is saved in the json file
        defaultSettingsPath = "/full/path/to/default/settings/file.json",
        userSettingsPath    = "/full/path/to/user/settings/file.json"
    }.Reset();

*/
public class PersistentSettingsManager {


    // Public Attributes
    // ======================================================================
    public object classInstance;
    public string defaultSettingsPath;
    public string userSettingsPath;
    public readonly string version = "1.0";
    // ======================================================================


    // Public Methods
    // ======================================================================

    public void Load() {
        JsonUtility.FromJsonOverwrite(File.ReadAllText(defaultSettingsPath), classInstance);

        if (File.Exists(userSettingsPath)) {
            JsonUtility.FromJsonOverwrite(File.ReadAllText(userSettingsPath), classInstance);
            Save();
        } else {
            Save();
        }
    } // Load


    public void Save() {
        File.WriteAllText(userSettingsPath, JsonUtility.ToJson(classInstance));
    } // Save


    public void Reset() {
        JsonUtility.FromJsonOverwrite(File.ReadAllText(defaultSettingsPath), classInstance);
        Save();
    } // Save

    // ======================================================================


}
